# Freedom

## A hugo Theme

![](https://gabe.rocks/themes/valor//media/freedom.webp)

### Why use these themes?

There are many wonderful themes on [themes.gohugo.io](https://themes.gohugo.io) that may suit your preferences. Our goal is to create themes that make it easy for users to make rich experiences, all without introducing unnecessary external dependencies.
The themes are designed with these goals in mind:

* Ease of use
* Ease of customization
* Supporting multimedia content
* Performance
* Sensible and minimal use of javascript
* Responsive
