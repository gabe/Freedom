<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom">
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
      <title>RSS</title>
      <link rel="stylesheet" href="/css/theme.css" />
      <style>
        body{
          background-image:url(/res/rss-light.webp);
          background-size:25rem;
          background-position: bottom left;
          background-repeat:no-repeat;
        }
        @media screen and (prefers-color-scheme:dark) {
          body{
            background-image:url(/res/rss-dark.webp);
          }
        }
      </style>
    </head>

    <body>
      <header>
        <nav>
          <a href="/">
            <h1>Home</h1>
          </a>
        </nav>
      </header>
      <article style="font-size:1.5rem;line-height:1.75rem;">
        <h1>This is an RSS Feed</h1>
        <p>
          You can copy <a href="">this link</a> and import it into your favorite RSS reader to read
          the posts at your own speed. This feed supports multimedia so podcasting apps will load the audio/video when there is media attached to a post.
        </p>
        <h4 style="margin:0;">Why bother?</h4>
        <p>
          An excellent way to directly fight censorship and democratize multimedia is to get your information from independent web sources using open protocols. Instead of relying on social media algorithms controlled by corporations you can curate your own experience. <br/><br/>You can learn more at <a href="https://indieweb.org/RSS">indieweb.org</a> and <a href="https://corbettreport.com/really-simple-syndication-solutionswatch/">The Corbett Report.</a>
        </p>
        <h3>Great RSS Readers</h3>
        <ul>
          <li><a href="https://antennapod.org/">AntennaPod</a> <i> For Android users, it's fantastic for multimedia!</i></li>
          <li><a href="https://github.com/martinrotter/rssguard">RSSGuard</a><i> A great option for desktop users</i></li>
          <li><a href="https://www.freshrss.org/">FreshRSS</a><i> This is an excellent option for somebody considering self-hosting.</i></li>
        </ul>
        <p>This page is made with <a href="https://gabe.rocks/tech/rss-love/">love for RSS</a></p>
        <hr/>

        <h3>Posts:</h3>
        <ul>
          <xsl:for-each select="/rss/channel/item">
            <a>
              <xsl:attribute name="href">
                <xsl:value-of select="link" />
              </xsl:attribute>
              <li style="margin-bottom:0.5rem;"><xsl:value-of select="title" /></li>
            </a>
          </xsl:for-each>
        </ul>
      </article>
      <footer style="flex-direction:column;">
        <h2>Want to make your feed look fancy?</h2>
        <p>Check out these posts:<br /><a href="https://sachachua.com/blog/2024/01/xml-stylesheet-for-my-feed/">Sacha
            Chua</a><br /><a href="https://darekkay.com/blog/rss-styling/">Darek Kay</a><br /><strong>Using
            hugo?</strong><br /> You're going to need to escape the xml-stylesheet tag <a
            href="https://code.gabe.rocks/gabriel/valor/src/branch/main/layouts/rss.xml">like this.</a> (first line)
        </p>
      </footer>

      
    </body>

    </html>
  </xsl:template>
</xsl:stylesheet>